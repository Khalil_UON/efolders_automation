﻿using CsvHelper;
using Microsoft.SharePoint.Client;
using OfficeDevPnP.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsRepositoryBuilder
{
    class Fix7DigitsStudentIDs
    {
        static void Main(string[] args)
        {

            //Load configuration ( site url / doc lib name, csv file)
            var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            var userName = ConfigurationManager.AppSettings["UserName"];
            var password = ConfigurationManager.AppSettings["Password"];
            var filePath = ConfigurationManager.AppSettings["StudentDataFilePath"];
            var documentLibraryName = ConfigurationManager.AppSettings["documentLibraryName"];


            AuthenticationManager authManager = new AuthenticationManager();

            StreamReader sr = new StreamReader(filePath);
            var csv = new CsvReader(sr);
            csv.Read();


            // Connect to SPO
            Console.WriteLine("Connecting to PnP Online");
            using (var clientContext = authManager.GetSharePointOnlineAuthenticatedContextTenant(siteUrl, userName, password))
            {
                //start checking the individual files
                while (csv.Read())
                {
                    var x = System.Diagnostics.Stopwatch.StartNew();
                    string fileNames = "";
                var studentId = csv.GetField<string>(0);
                    try
                    {
                        Folder studentDocumentsFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}", studentId, documentLibraryName));

                        var filesCollection = studentDocumentsFolder.FindFiles("*.*");
                        if (filesCollection.Count > 0)
                        {

                            filesCollection.ForEach(f => { fileNames += f.Name + " -"; });
                            System.IO.StreamWriter stream = System.IO.File.AppendText("log.txt");
                            stream.WriteLine(studentId);
                            stream.Flush();
                            stream.Close();
                            x.Stop();
                            Console.WriteLine(string.Format("{0} have {1} in {2}", studentId, fileNames, x.Elapsed));
                        }
                        else
                        {
                            //studentDocumentsFolder.DeleteObject();
                            //clientContext.ExecuteQuery();
                            x.Stop();
                            Console.WriteLine(string.Format("{0} is empty - to be deleted in {1} ", studentId, x.Elapsed));


                        }

                    }
                    catch(Exception ex)
                    {
                        string message = String.Format("Cannot find folder {0}", studentId);
                        System.IO.StreamWriter stream = System.IO.File.AppendText("log.txt");
                        stream.WriteLine(message);
                        stream.Flush();
                        stream.Close();

                    }
                   

                }
                Console.WriteLine("Finished All");

            }
        }
    }
}
