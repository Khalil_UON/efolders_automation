﻿using OfficeDevPnP.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.IO;
using System.Configuration;
using Microsoft.SharePoint.Client;

namespace DocumentsRepositoryBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            var userName = ConfigurationManager.AppSettings["UserName"];
            var password = ConfigurationManager.AppSettings["Password"];
            var filePath = ConfigurationManager.AppSettings["StudentDataFilePath"];
            var documentLibraryName = ConfigurationManager.AppSettings["documentLibraryName"];
            //string[] folderNames ConfigurationManager.AppSettings["Password"];= ["Attendance", "Applications", "Criminal Convictions and DBS", "OH"];

            AuthenticationManager authManager = new AuthenticationManager();

            StreamReader sr = new StreamReader(filePath);
            var csv = new CsvReader(sr);
            csv.Read();




              
                    

                    Console.WriteLine("Connecting to PnP Online");
                    using (var clientContext = authManager.GetSharePointOnlineAuthenticatedContextTenant(siteUrl, userName, password))
                    {

                    while (csv.Read())
                    {
                    var x = System.Diagnostics.Stopwatch.StartNew();
                        var studentId = csv.GetField<string>(0);
                    try
                    {
                       

                        Folder attendanceFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/attendance", studentId, documentLibraryName, documentLibraryName));
                        Folder applicationsFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/applications", studentId, documentLibraryName));
                        Folder dbsFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Criminal Convictions and DBS", studentId, documentLibraryName));
                        Folder ohFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/OH", studentId, documentLibraryName));
                        Folder enrolmentFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Enrolment and re-enrolment", studentId, documentLibraryName));
                        Folder DisabilityFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Disability", studentId, documentLibraryName));
                        Folder appealsFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Student Conduct - Complaints and appeals", studentId, documentLibraryName));
                        Folder migatingFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Mitigating Circumstances", studentId, documentLibraryName));
                        Folder consentFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Consent to Share", studentId, documentLibraryName));
                        Folder finanialFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Financial Matters and Issues", studentId, documentLibraryName));
                        Folder supportFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/International support and compliance", studentId, documentLibraryName));
                        Folder aplFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/APL", studentId, documentLibraryName));
                        Folder placementFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Placements and WBL", studentId, documentLibraryName));
                        Folder academicInfoFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Academic Information", studentId, documentLibraryName));
                        Folder counsellingFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Counselling", studentId, documentLibraryName));
                        Folder healthDropInsFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Counselling and mental health drop-ins", studentId, documentLibraryName));
                        Folder mentalHealthFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Mental Health", studentId, documentLibraryName));
                        Folder safegurdingFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Safeguarding", studentId, documentLibraryName));
                        Folder holdingFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Holding", studentId, documentLibraryName));
                        Folder notesFolder = clientContext.Site.RootWeb.GetFolderByServerRelativeUrl(string.Format("/sites/efolder/{1}/{0}/Student Support Notes", studentId, documentLibraryName));

                        var eFolderOwners = clientContext.Web.SiteGroups.GetByName("Student eFolder Owners");
                        var attendanceContributor = clientContext.Web.SiteGroups.GetByName("Attendance Contribute");
                        var applicationContributor = clientContext.Web.SiteGroups.GetByName("Applications Contribute");
                        var dbsContributor = clientContext.Web.SiteGroups.GetByName("Criminal Convictions and DBS Contribute");
                        var ohContributor = clientContext.Web.SiteGroups.GetByName("OH Contribute");
                        var enrollmentContributor = clientContext.Web.SiteGroups.GetByName("Enrolment and re-enrolment Contribute");
                        var disabilityContributor = clientContext.Web.SiteGroups.GetByName("Disability Contribute");
                        var appealContributor = clientContext.Web.SiteGroups.GetByName("Student Conduct - Complaints and appeals Contribute");
                        var mitigtatingContributor = clientContext.Web.SiteGroups.GetByName("Mitigating Circumstances Contribute");
                        var concentContributor = clientContext.Web.SiteGroups.GetByName("Consent to Share Contribute");
                        var financialContributor = clientContext.Web.SiteGroups.GetByName("Financial Matters and Issues Contribute");
                        var complianceContributor = clientContext.Web.SiteGroups.GetByName("International support and compliance Contribute");
                        var aplContributor = clientContext.Web.SiteGroups.GetByName("APL Contribute");
                        var placementContributor = clientContext.Web.SiteGroups.GetByName("Placements and WBL Contribute");
                        var academicContributor = clientContext.Web.SiteGroups.GetByName("Academic Information Contribute");
                        var counsellingContributor = clientContext.Web.SiteGroups.GetByName("Counselling Contribute");
                        var counsellingViewer = clientContext.Web.SiteGroups.GetByName("Counselling View");
                        var counsellingAdmin = clientContext.Web.SiteGroups.GetByName("Counselling Full Control");
                        var cnmContributor = clientContext.Web.SiteGroups.GetByName("Counselling and mental health drop-ins Contribute");
                        var cnmView = clientContext.Web.SiteGroups.GetByName("Counselling and mental health drop-ins View");
                        var cnmAdmin = clientContext.Web.SiteGroups.GetByName("Counselling and mental health drop-ins Full Control");
                        var mentalContributor = clientContext.Web.SiteGroups.GetByName("Mental Health Contribute");
                        var mentalViewer = clientContext.Web.SiteGroups.GetByName("Mental Health View");
                        var mentalAdmin = clientContext.Web.SiteGroups.GetByName("Mental Health Full Control");
                        var safegaurdContributor = clientContext.Web.SiteGroups.GetByName("Safeguarding Contribute");
                        var holdingContributor = clientContext.Web.SiteGroups.GetByName("Holding Contribute");
                        var supportContributor = clientContext.Web.SiteGroups.GetByName("Student Support Notes Contribute");

                        clientContext.Load(attendanceFolder);
                        clientContext.Load(applicationsFolder);
                        clientContext.Load(dbsFolder);
                        clientContext.Load(ohFolder);
                        clientContext.Load(enrolmentFolder);
                        clientContext.Load(DisabilityFolder);
                        clientContext.Load(appealsFolder);
                        clientContext.Load(migatingFolder);
                        clientContext.Load(consentFolder);
                        clientContext.Load(finanialFolder);
                        clientContext.Load(supportFolder);
                        clientContext.Load(aplFolder);
                        clientContext.Load(placementFolder);
                        clientContext.Load(academicInfoFolder);
                        clientContext.Load(counsellingFolder);
                        clientContext.Load(healthDropInsFolder);
                        clientContext.Load(mentalHealthFolder);
                        clientContext.Load(holdingFolder);
                        clientContext.Load(notesFolder);


                        clientContext.Load(eFolderOwners);
                        clientContext.Load(attendanceContributor);
                        clientContext.Load(dbsContributor);
                        clientContext.Load(ohContributor);
                        clientContext.Load(enrollmentContributor);
                        clientContext.Load(disabilityContributor);
                        clientContext.Load(appealContributor);
                        clientContext.Load(mitigtatingContributor);
                        clientContext.Load(concentContributor);
                        clientContext.Load(financialContributor);
                        clientContext.Load(complianceContributor);
                        clientContext.Load(aplContributor);
                        clientContext.Load(placementContributor);
                        clientContext.Load(academicContributor);
                        clientContext.Load(counsellingContributor);
                        clientContext.Load(counsellingViewer);
                        clientContext.Load(counsellingAdmin);
                        clientContext.Load(cnmContributor);
                        clientContext.Load(cnmView);
                        clientContext.Load(cnmAdmin);
                        clientContext.Load(mentalContributor);
                        clientContext.Load(mentalViewer);
                        clientContext.Load(mentalAdmin);
                        clientContext.Load(safegaurdContributor);
                        clientContext.Load(holdingContributor);
                        clientContext.Load(supportContributor);



                        clientContext.ExecuteQueryRetry(300, 10000);


                        /*
                         * Student eFolder Owners
                         * Attendance Contribute
                         * Applications Contribute
                         * Criminal Convictions and DBS Contribute
                         * OH Contribute
                         * Enrolment and re-enrolment Contribute
                         * Disability Contribute
                         * Student Conduct - Complaints and appeals Contribute
                         * Mitigating Circumstances Contribute
                         * Consent to Share Contribute
                         * Financial Matters and Issues Contribute
                         * International support and compliance Contribute
                         * APL Contribute
                         * Placements and WBL Contribute
                         * Academic Information Contribute
                         * Counselling Contribute
                         * Counselling View
                         * Counselling Full Control
                         * Counselling and mental health drop-ins Contribute
                         * Counselling and mental health drop-ins View
                         * Counselling and mental health drop-ins Full Control
                         * Mental Health Contribute
                         * Mental Health View
                         * Mental Health Full Control
                         * Safeguarding Contribute
                         * Holding Contribute
                         * Student Support Notes Contribute
                         */

                        var contributeRoleDefinition = clientContext.Web.RoleDefinitions.GetByType(RoleType.Contributor);
                        var fullControlRoleDefinition = clientContext.Web.RoleDefinitions.GetByType(RoleType.Administrator);
                        var viewRoleDefinition = clientContext.Web.RoleDefinitions.GetByType(RoleType.Reader);

                        RoleDefinitionBindingCollection contributeRoleDefinitionBinding = new RoleDefinitionBindingCollection(clientContext);
                        contributeRoleDefinitionBinding.Add(contributeRoleDefinition);

                        RoleDefinitionBindingCollection fullControlRoleDefinitionBinding = new RoleDefinitionBindingCollection(clientContext);
                        fullControlRoleDefinitionBinding.Add(fullControlRoleDefinition);

                        RoleDefinitionBindingCollection viewerRoleDefinitionBinding = new RoleDefinitionBindingCollection(clientContext);
                        fullControlRoleDefinitionBinding.Add(viewRoleDefinition);


                        attendanceFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        attendanceFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        attendanceFolder.ListItemAllFields.RoleAssignments.Add(attendanceContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });


                        applicationsFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        applicationsFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        applicationsFolder.ListItemAllFields.RoleAssignments.Add(applicationContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });


                        ohFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        ohFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        ohFolder.ListItemAllFields.RoleAssignments.Add(ohContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        dbsFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        dbsFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        dbsFolder.ListItemAllFields.RoleAssignments.Add(dbsContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        enrolmentFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        enrolmentFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        enrolmentFolder.ListItemAllFields.RoleAssignments.Add(enrollmentContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        DisabilityFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        DisabilityFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        DisabilityFolder.ListItemAllFields.RoleAssignments.Add(disabilityContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });


                        appealsFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        appealsFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        appealsFolder.ListItemAllFields.RoleAssignments.Add(appealContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        migatingFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        migatingFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        migatingFolder.ListItemAllFields.RoleAssignments.Add(mitigtatingContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        consentFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        consentFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        consentFolder.ListItemAllFields.RoleAssignments.Add(concentContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        finanialFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        finanialFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        finanialFolder.ListItemAllFields.RoleAssignments.Add(financialContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        supportFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        supportFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        supportFolder.ListItemAllFields.RoleAssignments.Add(complianceContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        aplFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        aplFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        aplFolder.ListItemAllFields.RoleAssignments.Add(aplContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        placementFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        placementFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        placementFolder.ListItemAllFields.RoleAssignments.Add(placementContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        academicInfoFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        academicInfoFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        academicInfoFolder.ListItemAllFields.RoleAssignments.Add(academicContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        counsellingFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        counsellingFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        counsellingFolder.ListItemAllFields.RoleAssignments.Add(counsellingContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });
                        counsellingFolder.ListItemAllFields.RoleAssignments.Add(counsellingAdmin, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        counsellingFolder.ListItemAllFields.RoleAssignments.Add(counsellingViewer, new RoleDefinitionBindingCollection(clientContext) { viewRoleDefinition });

                        healthDropInsFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        healthDropInsFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        healthDropInsFolder.ListItemAllFields.RoleAssignments.Add(cnmContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });
                        healthDropInsFolder.ListItemAllFields.RoleAssignments.Add(cnmAdmin, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        healthDropInsFolder.ListItemAllFields.RoleAssignments.Add(cnmView, new RoleDefinitionBindingCollection(clientContext) { viewRoleDefinition });

                        mentalHealthFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        mentalHealthFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        mentalHealthFolder.ListItemAllFields.RoleAssignments.Add(mentalContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });
                        mentalHealthFolder.ListItemAllFields.RoleAssignments.Add(mentalAdmin, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        mentalHealthFolder.ListItemAllFields.RoleAssignments.Add(mentalViewer, new RoleDefinitionBindingCollection(clientContext) { viewRoleDefinition });

                        safegurdingFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        safegurdingFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        safegurdingFolder.ListItemAllFields.RoleAssignments.Add(safegaurdContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        holdingFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        holdingFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        holdingFolder.ListItemAllFields.RoleAssignments.Add(holdingContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        notesFolder.ListItemAllFields.BreakRoleInheritance(false, true);
                        notesFolder.ListItemAllFields.RoleAssignments.Add(eFolderOwners, new RoleDefinitionBindingCollection(clientContext) { fullControlRoleDefinition });
                        notesFolder.ListItemAllFields.RoleAssignments.Add(supportContributor, new RoleDefinitionBindingCollection(clientContext) { contributeRoleDefinition });

                        clientContext.ExecuteQueryRetry(300, 10000);

                        x.Stop();



                       
                        Console.WriteLine("Setting permissions finished for  " + studentId + " in " + x.Elapsed);
                        //Console.Read();

                    }

                    catch (Exception ex)
                    {
                        System.IO.StreamWriter stream = System.IO.File.AppendText("log.txt");
                        stream.WriteLine(studentId);
                        stream.Flush();
                        stream.Close();
                        Console.WriteLine("Error Message: " + ex.Message);
                        //Console.ReadKey();
                    }

                   
                }



                Console.WriteLine("Finished all Records");
                Console.Read();
            }
          

            // }



            //            try
            //{
            //                // Get and set the client context  
            //                // Connects to SharePoint online site using inputs provided  
            //                using (var clientContext = authManager.GetSharePointOnlineAuthenticatedContextTenant(siteUrl, userName, password))
            //                {
            //                    // List Name input  
            //                    //string listName = "PnPCSOMList";
            //                    //// Retrieves list object using title  
            //                    //List list = clientContext.Site.RootWeb.GetListByTitle(listName);
            //                    //if (list != null)
            //                    //{
            //                    //    // Displays required result  
            //                    //    Console.WriteLine("List Title : " + list.Title);
            //                    //}
            //                    //else
            //                    //{
            //                    //    Console.WriteLine("List is not available on the site");
            //                    //}
            //                    //Console.ReadKey();
            //                }
            //            }
            //catch (Exception ex)
            //            {
            //                //Console.WriteLine("Error Message: " + ex.Message);
            //                //Console.ReadKey();
            //            }
            //Connect to SPO
            //Load first 100 Students from the spreadsheet
            //Create a Student Document Set using Student ID
            //Create Subfolders
            //Setup Permission
        }
    }
}
